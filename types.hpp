#ifndef __SLAVE_TYPES_HPP
#define __SLAVE_TYPES_HPP

#include <cinttypes>
#include <ctime>
#include <string>

namespace slave
{
namespace types
{
    typedef uint32_t            MY_INT;
    typedef unsigned long long  MY_BIGINT;
    typedef uint32_t            MY_MEDIUMINT;
    typedef uint16_t            MY_SMALLINT;
    typedef char                MY_TINYINT;
    typedef uint64_t            MY_BIT;
    typedef int                 MY_ENUM;
    typedef unsigned long long  MY_SET;
    typedef double              MY_DOUBLE;
    typedef double              MY_DECIMAL;
    typedef uint32_t            MY_DATE;
    typedef int32_t             MY_TIME;
    typedef unsigned long long  MY_DATETIME;
    typedef uint32_t            MY_TIMESTAMP;
    typedef std::string         MY_CHAR;
    typedef std::string         MY_VARCHAR;
    typedef std::string         MY_TINYTEXT;
    typedef std::string         MY_TEXT;
    typedef std::string         MY_BLOB;

    // MY_TIME value is represented as a 32-bit number similar to this: [+/-]HHHMMSS (HHH - hours, MM - minutes, SS - seconds)
    // with range from -8385959 to 8385959
}// types
}// slave

#endif
