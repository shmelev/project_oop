ABOUT
===================================================================

This is a library that allows C++ application to connect
to a MySQL replication master and read/parse the replication binary
logs. One important application for this library is for receiving changes in
the master database in real-time, without having the store the
master's data on the client server.

Features
-------------------------------------------------------------------
* Statistics of rps and time of every event in every table.
* Support for mysql options:
  * binlog_checksum=(NONE,CRC32)
  * binlog_row_image=(full,minimal)

USAGE
===================================================================

Build requirements
-------------------------------------------------------------------

For building the library, you will need:

 * g++ >= 4.9.2.

 * cmake >= 3.0

 * The standard mysql C client libraries (libmysqlclient):

 * The headers of the boost libraries (http://www.boost.org) >= 1.55.
   If boost_unit_test_framework is found, tests will be built.

Usage requirements
-------------------------------------------------------------------
 * Requires >= MySQL 5.5 and <= MySQL 5.7.12.
 * Set server_id=1, log_bin, binlog_format=ROW

Compiling
-------------------------------------------------------------------

 * `mkdir build && cd build`
 * `cmake ..`
 * `make -j$(nproc)`
 * `make test`
 * `./test/test_client -h`

Using the library
-------------------------------------------------------------------

Please see the examples in 'test/'.
