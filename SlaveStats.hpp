#ifndef __SLAVE_SLAVESTATS_HPP_
#define __SLAVE_SLAVESTATS_HPP_

#include <memory>
#include <string>
#include <sys/time.h>


namespace slave
{

enum enum_binlog_checksum_alg
{
    BINLOG_CHECKSUM_ALG_OFF = 0,
    BINLOG_CHECKSUM_ALG_CRC32 = 1,
    BINLOG_CHECKSUM_ALG_ENUM_END,
    BINLOG_CHECKSUM_ALG_UNDEF = 255
};

struct MasterInfo
{
    std::string host;
    unsigned int port;
    std::string user;
    std::string password;
    std::string master_log_name;
    unsigned long master_log_pos;
    unsigned int connect_retry;
    enum_binlog_checksum_alg checksum_alg = BINLOG_CHECKSUM_ALG_OFF;
    bool is_old_storage = true;

    MasterInfo() : port(3306), master_log_pos(0), connect_retry(10) {}

    MasterInfo(std::string host_, unsigned int port_, std::string user_,
               std::string password_, unsigned int connect_retry_)
        : host(host_)
        , port(port_)
        , user(user_)
        , password(password_)
        , master_log_name()
        , master_log_pos(0)
        , connect_retry(connect_retry_)
    {}

    bool checksumEnabled() const { return checksum_alg == BINLOG_CHECKSUM_ALG_CRC32; }
};

struct State
{
    time_t          connect_time;
    time_t          last_filtered_update;
    time_t          last_event_time;
    time_t          last_update;
    std::string     master_log_name;
    unsigned long   master_log_pos;
    unsigned long   intransaction_pos;
    unsigned int    connect_count;
    bool            state_processing;

    State() :
        connect_time(0),
        last_filtered_update(0),
        last_event_time(0),
        last_update(0),
        master_log_pos(0),
        intransaction_pos(0),
        connect_count(0),
        state_processing(false)
    {}
};

struct ExtStateIface
{
    virtual State getState() = 0;
    virtual void setConnecting() = 0;
    virtual time_t getConnectTime() = 0;
    virtual void setLastFilteredUpdateTime() = 0;
    virtual time_t getLastFilteredUpdateTime() = 0;
    virtual void setLastEventTimePos(time_t t, unsigned long pos) = 0;
    virtual time_t getLastUpdateTime() = 0;
    virtual time_t getLastEventTime() = 0;
    virtual unsigned long getIntransactionPos() = 0;
    virtual void setMasterLogNamePos(const std::string& log_name, unsigned long pos) = 0;
    virtual unsigned long getMasterLogPos() = 0;
    virtual std::string getMasterLogName() = 0;

    virtual void saveMasterInfo() = 0;

    virtual bool loadMasterInfo(std::string& logname, unsigned long& pos) = 0;

    // Works like loadMasterInfo() but writes last position inside transaction if presented.
    bool getMasterInfo(std::string& logname, unsigned long& pos)
    {
        unsigned long in_trans_pos = getIntransactionPos();
        std::string master_logname = getMasterLogName();

        if (in_trans_pos != 0 && !master_logname.empty())
        {
            logname = master_logname;
            pos = in_trans_pos;
            return true;
        }
        return loadMasterInfo(logname, pos);
    }
    virtual unsigned int getConnectCount() = 0;
    virtual void setStateProcessing(bool _state) = 0;
    virtual bool getStateProcessing() = 0;
    // There is no standard format for events distribution in the tables,
    // so there is no function for getting this statistics.
    virtual void initTableCount(const std::string& t) = 0;
    virtual void incTableCount(const std::string& t) = 0;

    virtual ~ExtStateIface() {}
};

// Stub object for answers on stats requests through StateHolder while libslave is not initialized yet.
struct EmptyExtState: public ExtStateIface
{
    EmptyExtState() : master_log_pos(0), intransaction_pos(0) {}

    virtual State getState() { return State(); }
    virtual void setConnecting() {}
    virtual time_t getConnectTime() { return 0; }
    virtual void setLastFilteredUpdateTime() {}
    virtual time_t getLastFilteredUpdateTime() { return 0; }
    virtual void setLastEventTimePos(time_t t, unsigned long pos) { intransaction_pos = pos; }
    virtual time_t getLastUpdateTime() { return 0; }
    virtual time_t getLastEventTime() { return 0; }
    virtual unsigned long getIntransactionPos() { return intransaction_pos; }
    virtual void setMasterLogNamePos(const std::string& log_name, unsigned long pos) { master_log_name = log_name; master_log_pos = intransaction_pos = pos;}
    virtual unsigned long getMasterLogPos() { return master_log_pos; }
    virtual std::string getMasterLogName() { return master_log_name; }
    virtual void saveMasterInfo() {}
    virtual bool loadMasterInfo(std::string& logname, unsigned long& pos) { logname.clear(); pos = 0; return false; }
    virtual unsigned int getConnectCount() { return 0; }
    virtual void setStateProcessing(bool _state) {}
    virtual bool getStateProcessing() { return false; }
    virtual void initTableCount(const std::string& t) {}
    virtual void incTableCount(const std::string& t) {}

private:
    std::string     master_log_name;
    unsigned long   master_log_pos;
    unsigned long   intransaction_pos;
};

enum EventKind
{
    eNone   = 0,
    eInsert = (1 << 0),
    eUpdate = (1 << 1),
    eDelete = (1 << 2),
    eAll    = (1 << 0) | (1 << 1) | (1 << 2)
};

typedef EventKind EventKindList[3];

inline const EventKindList& eventKindList()
{
    static EventKindList result = {eInsert, eUpdate, eDelete};
    return result;
}

// All stats calls are called independently.
// E. g., processing UPDATE on a table, tick() + one of tickModifyIgnored/tickModifyDone/tickModifyFailed will be called.
class EventStatIface
{
public:
    virtual ~EventStatIface() {}
    // TABLE_MAP event.
    virtual void processTableMap(const unsigned long /*id*/, const std::string& /*table*/, const std::string& /*database*/) {}
    // All events.
    virtual void tick(time_t when) {}
    // FORMAT_DESCRIPTION events.
    virtual void tickFormatDescription() {}
    // QUERY events.
    virtual void tickQuery() {}
    // ROTATE events.
    virtual void tickRotate() {}
    // XID events.
    virtual void tickXid() {}
    // Unprocessed libslave events.
    virtual void tickOther() {}
    // UPDATE/INSERT/DELETE missed (there are not callbacks on given type of operation).
    virtual void tickModifyIgnored(const unsigned long /*id*/, EventKind /*kind*/) {}
    // UPDATE/INSERT/DELETE successfully processed.
    virtual void tickModifyDone(const unsigned long /*id*/, EventKind /*kind*/, uint64_t /*callbackWorkTimeNanoSeconds*/) {}
    // UPDATE/INSERT/DELETE processed with errors (caught exception).
    virtual void tickModifyFailed(const unsigned long /*id*/, EventKind /*kind*/, uint64_t /*callbackWorkTimeNanoSeconds*/) {}
    // UPDATE/INSERT/DELETE rows successfully processed (Modify event may affect several rows of table).
    virtual void tickModifyRow() {}
    // Errors during processing
    virtual void tickError() {}
};
}

#endif
