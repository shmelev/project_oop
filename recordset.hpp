#ifndef __SLAVE_RECORDSET_HPP_
#define __SLAVE_RECORDSET_HPP_

#include <map>
#include <string>

#include <boost/any.hpp>

namespace slave
{

// One row in a table. Key -- field name, value - pair of (field type, value)
typedef std::map<std::string, std::pair<std::string, boost::any> > Row;

struct RecordSet
{
    Row m_row, m_old_row;

    std::string tbl_name;
    std::string db_name;

    time_t when;

    enum TypeEvent { Update, Delete, Write };

    TypeEvent type_event;

    unsigned int master_id;
    RecordSet(): master_id(0) {}
};

}// slave

#endif
