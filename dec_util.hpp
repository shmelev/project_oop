#ifndef __SLAVE_DEC_UTIL_HPP_
#define __SLAVE_DEC_UTIL_HPP_

#include <mysql/my_global.h>
#include <mysql/decimal.h>

namespace slave
{

namespace dec_util
{

typedef decimal_digit_t dec1;

// converts from raw const char* to internal MySQL type decimal_t
int bin2dec(const char *from, decimal_t *to, int precision, int scale);

// converts from internal MySQL type decimal_t to double
void dec2dbl(decimal_t *from, double *to);

} // namespace dec_util

} // namespace slave

#endif
