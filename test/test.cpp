#include <iostream>
#include <signal.h>
#include <sstream>
#include <unistd.h>

#include "DefaultExtState.hpp"
#include "Slave.hpp"


volatile sig_atomic_t stop = 0;
slave::Slave* sl = nullptr;

std::string print(const std::string& type, const boost::any& v)
{
    if (v.type() == typeid(std::string))
    {
        std::string r = "'";
        r += boost::any_cast<std::string>(v);
        r += "'";
        return r;

    }
    else
    {
        std::ostringstream s;

        if (v.type() == typeid(int))
            s << boost::any_cast<int>(v);
        else if (v.type() == typeid(unsigned int))
            s << boost::any_cast<unsigned int>(v);
        else if (v.type() == typeid(double))
            s << boost::any_cast<double>(v);
        else if (v.type() == typeid(unsigned long long))
            s << boost::any_cast<unsigned long long>(v);
        else if (v.type() == typeid(void))
            s << "void";
        else
            s << boost::any_cast<long>(v);
        return s.str();
    }
}


void callback(const slave::RecordSet& event)
{
    slave::Slave::binlog_pos_t sBinlogPos = sl->getLastBinlog();
    std::cout << "master pos " << sBinlogPos.first << " : " << sBinlogPos.second << std::endl;

    switch (event.type_event)
    {
    case slave::RecordSet::Update: std::cout << "UPDATE"; break;
    case slave::RecordSet::Delete: std::cout << "DELETE"; break;
    case slave::RecordSet::Write:  std::cout << "INSERT"; break;
    default: break;
    }

    std::cout << " " << event.db_name << "." << event.tbl_name << "\n";

    for (slave::Row::const_iterator i = event.m_row.begin(); i != event.m_row.end(); ++i)
    {
        std::string value = print(i->second.first, i->second.second);
        std::cout << "  " << i->first << " : " << i->second.first << " -> " << value;

        if (event.type_event == slave::RecordSet::Update)
        {
            slave::Row::const_iterator j = event.m_old_row.find(i->first);
            std::string old_value("NULL");
            if (j != event.m_old_row.end())
                old_value = print(i->second.first, j->second.second);

            if (value != old_value)
                std::cout << "    (was: " << old_value << ")";
        }
        std::cout << "\n";
    }

    std::cout << "  @ts = "  << event.when << "\n"
              << "  @server_id = " << event.master_id << "\n\n";
}

void xid_callback(unsigned int server_id)
{
    std::cout << "COMMIT @server_id = " << server_id << "\n\n";
}

void sighandler(int sig)
{
    stop = 1;
    sl->close_connection();
}

bool isStopping()
{
    return stop;
}

void usage(const char* name)
{
    std::cout << "Usage: " << name << " -h <mysql host> -u <mysql user> -p <mysql password> -d <mysql database>"
              << " -b <binlog_name> -o <binlog_pos> -B <to_binlog_name> -O <to_binlog_pos>"
              << " <table name> <table name> ...\n"
              << "\n";
}

int main(int argc, char** argv)
{
    std::string host;
    std::string user;
    std::string password;
    std::string database;
    unsigned int port = 3306;

    std::string binlog_name;
    unsigned long binlog_pos = 0;
    std::string to_binlog_name;
    unsigned long to_binlog_pos = 0;

    int c;
    while (-1 != (c = ::getopt(argc, argv, "h:u:p:P:d:b:o:B:O")))
    {
        switch (c)
        {
        case 'h': host = optarg; break;
        case 'u': user = optarg; break;
        case 'p': password = optarg; break;
        case 'd': database = optarg; break;
        case 'P': port = std::stoi(optarg); break;
        case 'b': binlog_name = optarg; break;
        case 'o': binlog_pos = std::stoul(optarg); break;
        case 'B': to_binlog_name = optarg; break;
        case 'O': to_binlog_pos = std::stoul(optarg); break;
        default:
            usage(argv[0]);
            return 1;
        }
    }

    if (host.empty() || user.empty() || database.empty())
    {
        usage(argv[0]);
        return 1;
    }

    std::vector<std::string> tables;

    while (optind < argc)
    {
        tables.push_back(argv[optind]);
        optind++;
    }

    slave::MasterInfo masterinfo;

    masterinfo.host = host;
    masterinfo.port = port;
    masterinfo.user = user;
    masterinfo.password = password;
    signal(SIGINT, sighandler);
    signal(SIGTERM, sighandler);

    bool error = false;

    try
    {
        std::cout << "Creating client, setting callbacks...\n";

        slave::DefaultExtState sDefExtState;
        slave::Slave slave(masterinfo, sDefExtState);
        sl = &slave;
        sDefExtState.setMasterLogNamePos(binlog_name, binlog_pos);

        for (const auto& t : tables)
            slave.setCallback(database, t, callback);

        slave.setXidCallback(xid_callback);

        std::cout << "Initializing client...\n";
        slave.init();

        std::cout << "Reading database structure...\n";
        slave.createDatabaseStructure();

        try
        {
            std::cout << "Reading binlogs...\n";
            if (!to_binlog_name.empty() || 0 != to_binlog_pos)
            {
                slave.get_remote_binlog([&] ()
                {
                    const slave::MasterInfo& sMasterInfo = slave.masterInfo();
                    return (isStopping()
                            || sMasterInfo.master_log_name > to_binlog_name
                            || (sMasterInfo.master_log_name == to_binlog_name
                            && sMasterInfo.master_log_pos >= to_binlog_pos));
                });
            }
            else
            {
                slave.get_remote_binlog(isStopping);
            }

        }
        catch (std::exception& ex)
        {
            std::cout << "Error in reading binlogs: " << ex.what() << "\n";
            error = true;
        }
    }
    catch (std::exception& ex)
    {
        std::cout << "Error in initializing slave: " << ex.what() << "\n";
        error = true;
    }

    return int(error);
}
