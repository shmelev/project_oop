#ifndef __SLAVE_TABLE_HPP_
#define __SLAVE_TABLE_HPP_

#include <functional>
#include <map>
#include <memory>
#include <string>
#include <vector>

#include "field.hpp"
#include "recordset.hpp"
#include "SlaveStats.hpp"


namespace slave
{

typedef std::shared_ptr<Field> PtrField;
typedef std::function<void (RecordSet&)> callback;
typedef EventKind filter;


inline bool should_process(EventKind filter, EventKind kind) { return (filter & kind) == kind; }

class Table
{
public:
    std::vector<PtrField> fields;

    callback m_callback;
    EventKind m_filter;

    void call_callback(RecordSet& record_set, ExtStateIface& ext_state)
    {
        ext_state.incTableCount(full_name);
        ext_state.setLastFilteredUpdateTime();

        m_callback(record_set);
    }

    const std::string table_name;
    const std::string database_name;

    std::string full_name;

    Table() {}

    Table(const std::string& db_name, const std::string& tbl_name)
        : table_name(tbl_name)
        , database_name(db_name)
        , full_name(database_name + "." + table_name)
    {}
};

}// namespace slave

#endif
