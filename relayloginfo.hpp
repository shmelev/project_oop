#ifndef __SLAVE_RELAYLOGINFO_HPP_
#define __SLAVE_RELAYLOGINFO_HPP_

#include <map>
#include <memory>
#include <string>

#include "table.hpp"


namespace slave
{

typedef std::shared_ptr<Table> PtrTable;

class RelayLogInfo
{
public:
    typedef std::map<unsigned long, std::pair<std::string, std::string>> id_to_name_t;
    id_to_name_t m_map_table_name;

    typedef std::map<std::pair<std::string, std::string>, PtrTable> name_to_table_t;
    name_to_table_t m_table_map;

    void clear()
    {
        m_map_table_name.clear();
        m_table_map.clear();
    }

    void setTableName(unsigned long table_id, const std::string& table_name, const std::string& db_name)
    {
        m_map_table_name[table_id] = std::make_pair(db_name, table_name);
    }

    const std::pair<std::string,std::string> getTableNameById(int table_id)
    {
        id_to_name_t::const_iterator p = m_map_table_name.find(table_id);

        if (p != m_map_table_name.end()) {
            return p->second;
        } else {
            return std::make_pair(std::string(), std::string());
        }
    }

    std::shared_ptr<Table> getTable(const std::pair<std::string, std::string>& key)
    {
        name_to_table_t::iterator p = m_table_map.find(key);

        if (p != m_table_map.end())
            return p->second;
        return std::shared_ptr<Table>();
    }

    void setTable(const std::string& table_name, const std::string& db_name, PtrTable table)
    {
        m_table_map[std::make_pair(db_name, table_name)] = table;
    }
};

}// namespace slave

#endif
